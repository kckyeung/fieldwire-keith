class AddProjectIdToFloorPlans < ActiveRecord::Migration[5.0]
  def change
    add_column :floor_plans, :project_id, :integer
    add_foreign_key :floor_plans, :projects
  end
end
