class RemoveColumnsInProjects < ActiveRecord::Migration[5.0]
  def change
    change_table :projects do |t|
      t.remove :display_name
      t.remove :blueprint
    end
  end
end
