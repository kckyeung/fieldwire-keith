class CreateFloorPlans < ActiveRecord::Migration[5.0]
  def change
    create_table :floor_plans do |t|
      t.string :display_name
      t.string :blueprint

      t.timestamps
    end
  end
end
