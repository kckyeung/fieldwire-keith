#[macro_use]
extern crate ruru;
extern crate magick_rust;
extern crate aws_sdk_rust;

use aws_sdk_rust::aws::common::credentials::DefaultCredentialsProvider;
use aws_sdk_rust::aws::common::region::Region;
use aws_sdk_rust::aws::s3::acl::CannedAcl;
use aws_sdk_rust::aws::s3::endpoint::{Endpoint, Signature};
use aws_sdk_rust::aws::s3::s3client::S3Client;
use aws_sdk_rust::aws::s3::object::PutObjectRequest;
use ruru::{Boolean, Class, Object, NilClass, RString};
use magick_rust::{MagickWand, magick_wand_genesis};
use std::thread;

class!(RuMagick);

// Normally, calling .unwrap() is not advised because it will cause the thread to panic, but since
// we know that only child threads are going to run these functions, the JoinHandles would just
// return an error result. We check that in convert_and_upload.
fn thumb_convert_and_upload(path: &str, project_name: &str, filename: &str) {
    let wand = MagickWand::new();
    wand.read_image(&path).unwrap();
    wand.fit(100, 100);
    let blob = wand.write_image_blob("png").unwrap();
    let provider = DefaultCredentialsProvider::new(None).unwrap();

    let endpoint = Endpoint::new(Region::UsWest1, Signature::V4, None, None, None, None);
    let client = S3Client::new(provider, endpoint);

    let mut put_object = PutObjectRequest::default();
    put_object.bucket = "kichjang".to_string();
    put_object.key = format!("{}/{}_thumb.png", project_name, filename);
    put_object.body = Some(&blob);
    put_object.acl = Some(CannedAcl::PublicRead);

    client.put_object(&put_object, None).unwrap();
}

fn large_png_convert_and_upload(path: &str, project_name: &str, filename: &str) {
    let wand = MagickWand::new();
    wand.read_image(&path).unwrap();
    wand.fit(2000, 2000);
    let blob = wand.write_image_blob("png").unwrap();
    let provider = DefaultCredentialsProvider::new(None).unwrap();

    let endpoint = Endpoint::new(Region::UsWest1, Signature::V4, None, None, None, None);
    let client = S3Client::new(provider, endpoint);

    let mut put_object = PutObjectRequest::default();
    put_object.bucket = "kichjang".to_string();
    put_object.key = format!("{}/{}_large.png", project_name, filename);
    put_object.body = Some(&blob);
    put_object.acl = Some(CannedAcl::PublicRead);

    client.put_object(&put_object, None).unwrap();
}

methods!(
    RuMagick,
    _itself,

    fn init() -> NilClass {
        magick_wand_genesis();
        NilClass::new()
    }

    fn convert_and_upload(path: RString, project_name: RString, filename: RString) -> Boolean {
        let path_str = match path {
            Ok(s) => s.to_string(),
            Err(_) => return Boolean::new(false),
        };
        let prj_str = match project_name {
            Ok(s) => s.to_string(),
            Err(_) => return Boolean::new(false),
        };
        let file_str = match filename {
            Ok(s) => s.to_string(),
            Err(_) => return Boolean::new(false),
        };

        let path = path_str.clone();
        let project_name = prj_str.clone();
        let filename = file_str.clone();

        // Spawn 2 threads separately and immediately start execution
        let thumb_thread = thread::spawn(move || {
            thumb_convert_and_upload(&path, &project_name, &filename);
        });
        let large_thread = thread::spawn(move || {
            large_png_convert_and_upload(&path_str, &prj_str, &file_str);
        });

        // Wait for the thread to finish, and check if it returned any errors
        if thumb_thread.join().is_err() {
            return Boolean::new(false);
        }
        // If the thumb_thread took a longer time, the following check would run immediately,
        // otherwise we wait for the large_thread to finish.
        //
        // Essentially, we are paying only for the more time-consuming thread due to parallelism.
        if large_thread.join().is_err() {
            return Boolean::new(false);
        }

        Boolean::new(true)
    }
);

#[no_mangle]
pub extern fn initialize_rumagick() {
    Class::new("RuMagick", None).define(|itself| {
        itself.def_self("init", init);
        itself.def_self("convert_and_upload", convert_and_upload);
    });
}
