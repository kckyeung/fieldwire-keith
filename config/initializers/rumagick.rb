require 'fiddle'

library = Fiddle::dlopen('lib/assets/librumagick.so')
Fiddle::Function.new(library['initialize_rumagick'], [], Fiddle::TYPE_VOIDP).call
RuMagick::init
