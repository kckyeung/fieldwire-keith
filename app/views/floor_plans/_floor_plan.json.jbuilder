json.extract! floor_plan, :id, :display_name, :blueprint, :created_at, :updated_at
json.url floor_plan_url(floor_plan, format: :json)