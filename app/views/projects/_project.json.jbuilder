json.extract! project, :id, :name, :display_name, :blueprint, :created_at, :updated_at
json.url project_url(project, format: :json)