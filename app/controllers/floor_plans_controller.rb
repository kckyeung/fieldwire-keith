class FloorPlansController < ApplicationController
  before_action :set_floor_plan, only: [:show, :edit, :update, :destroy]

  # GET /floor_plans
  # GET /floor_plans.json
  def index
    project = Project.find(params[:project_id])
    @project_name = project.name
    @floor_plans = project.floor_plans
  end

  # GET /floor_plans/1
  # GET /floor_plans/1.json
  def show
    url_fragments = @floor_plan.blueprint.split('/')
    url_fragments.pop
    base_url = "#{url_fragments.join('/')}/#{@floor_plan.display_name}"
    @thumbnail_url = "#{base_url}_thumb.png"
    @large_url = "#{base_url}_large.png"
  end

  # GET /floor_plans/new
  def new
    @floor_plan = FloorPlan.new
  end

  # GET /floor_plans/1/edit
  def edit
  end

  # POST /floor_plans
  # POST /floor_plans.json
  def create
    project = Project.find(params[:project_id].to_i)

    @floor_plans = floor_plan_params[:files].map do |file|
      filename = file.original_filename
      display_name = floor_plan_params[:display_name].presence || filename.split('.')[0]

      # I've considered uploading this in RuMagick as well, but that would not return a public_url
      obj = S3_BUCKET.object("#{project.name}/#{filename}")
      obj.put({acl: "public-read", body: file})
      RuMagick::convert_and_upload(file.path, project.name, display_name)

      existing_plan = FloorPlan.find_by(display_name: display_name)
      if existing_plan.nil?
        attributes = {
          display_name: display_name,
          blueprint: obj.public_url,
          project_id: project.id
        }
        FloorPlan.new(attributes)
      else
        existing_plan.update(blueprint: obj.public_url)
        existing_plan
      end
    end

    respond_to do |format|
      if @floor_plans.map(&:save).all? { |e| e }
        format.html { redirect_to project_floor_plans_path, notice: 'Floor plans were successfully created.' }
        format.json { render :show, status: :created, location: @floor_plans }
      else
        format.html { render :new }
        format.json { render json: @floor_plans.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /floor_plans/1
  # PATCH/PUT /floor_plans/1.json
  def update
    respond_to do |format|
      if @floor_plan.update(floor_plan_params)
        format.html { redirect_to @floor_plan, notice: 'Floor plan was successfully updated.' }
        format.json { render :show, status: :ok, location: @floor_plan }
      else
        format.html { render :edit }
        format.json { render json: @floor_plan.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /floor_plans/1
  # DELETE /floor_plans/1.json
  def destroy
    @floor_plan.destroy
    respond_to do |format|
      format.html { redirect_to project_floor_plans_url(params[:project_id]), notice: 'Floor plan was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_floor_plan
      @floor_plan = FloorPlan.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def floor_plan_params
      params.require(:floor_plan).permit(:display_name, files: [])
    end
end
