# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

ready = ->
  $('#add-more').click () ->
    $('#display-name').remove()
    random = Math.random().toString(36).substring(2, 7)
    input = $('<input></input>').attr({ type: "file", name: "floor_plan[files][]"})
    $('#files').append(input)

$(document).on('turbolinks:load', ready)
