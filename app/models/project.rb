class Project < ApplicationRecord
  validates_presence_of :name
  has_many :floor_plans, dependent: :destroy
end
