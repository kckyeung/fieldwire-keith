class FloorPlan < ApplicationRecord
  validates_presence_of :display_name
  validates_format_of :blueprint, with: URI.regexp
  validates :project_id, presence: true, numericality: true
  belongs_to :project

  before_destroy :delete_objects_on_s3

  def delete_objects_on_s3
    project_name = self.project.name
    file_key = "#{project_name}/#{URI.decode(self.blueprint.split('/').pop)}"
    display_name = self.display_name
    thumbnail_key = "#{project_name}/#{display_name}_thumb.png"
    large_key = "#{project_name}/#{display_name}_large.png"

    S3_BUCKET.delete_objects({
      delete: {
        objects: [
          {
            key: file_key
          },
          {
            key: thumbnail_key
          },
          {
            key: large_key
          }
        ]
      }
    })
  end
end
