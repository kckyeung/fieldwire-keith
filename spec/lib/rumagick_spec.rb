require 'rails_helper'

RSpec.describe RuMagick do
  describe "Existence" do
    it "should have the init method" do
      expect(described_class).to respond_to(:init)
    end

    it "should have the convert_and_upload method" do
      expect(described_class).to respond_to(:convert_and_upload)
    end
  end

  describe "::convert_and_upload" do
    it "returns false with no arguments" do
      allow(described_class).to receive(:convert_and_upload).with(no_args).and_return(false)
    end

    it "returns false with 1 argument" do
      allow(described_class).to receive(:convert_and_upload).with(anything).and_return(false)
    end

    it "returns false with 2 arguments" do
      allow(described_class).to receive(:convert_and_upload).with(anything, anything).and_return(false)
    end

    it "returns false with 3 invalid arguments" do
      allow(described_class).to receive(:convert_and_upload).with(boolean, anything, anything).and_return(false)
      allow(described_class).to receive(:convert_and_upload).with(anything, boolean, anything).and_return(false)
      allow(described_class).to receive(:convert_and_upload).with(anything, anything, boolean).and_return(false)
    end
  end
end
