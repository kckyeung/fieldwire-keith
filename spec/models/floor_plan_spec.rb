require 'rails_helper'

RSpec.describe FloorPlan, type: :model do
  describe "Validations" do
    it { should validate_presence_of(:display_name) }
    it { should validate_presence_of(:project_id) }
    it { should validate_numericality_of(:project_id) }
  end

  describe "Associations" do
    it { should belong_to(:project) }
  end
end
